#include "Tour.h"

Tour::Tour(bool white, bool left) : Piece(white ? 1 : 8, left ? 1 : 8, white) {}

bool Tour::mouvementValide(Echiquier &e, int ligne, int colonne)
{
	int dx = colonne - m_colonne;
	int dy = ligne - m_ligne;
	return Piece::mouvementValide(e, ligne, colonne) && (dx == 0 || dy == 0);
}

char Tour::codePiece() const {
	return m_white ? 'T' : 't';
}