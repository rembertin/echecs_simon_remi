/**
 * Mise en oeuvre de Piece.h
 *
 * @file Piece.cxx
 */

// A besoin de la declaration de la classe
#include <iostream>
#include "Piece.h"
#include "Echiquier.h"

// Pour utiliser les flux de iostream sans mettre "std::" tout le temps.
using namespace std;

Piece::Piece() {}


Piece::Piece(const Piece &autre) {
	m_colonne = autre.m_colonne;
	m_ligne = autre.m_ligne;
	m_white = autre.m_white;
}

Piece & Piece::operator=(const Piece &autre) {
    m_ligne = autre.m_ligne;
    m_colonne = autre.m_colonne;
	m_white = autre.m_white;
	return *this;
}

Piece::~Piece() {}

Piece::Piece(int ligne, int colonne, bool white)
{
    m_ligne = ligne;
	m_colonne = colonne;
	m_white = white;
}

void Piece::move(int ligne, int colonne)
{
    m_ligne = ligne;
	m_colonne = colonne;
}

int Piece::getLigne() const {
    return m_ligne;
}

int Piece::getColonne() const {
	return m_colonne;
}

bool Piece::estBlanc() const {
	return m_white;
}

bool Piece::mouvementValide(Echiquier &e, int ligne, int colonne)
{
    if (colonne < 1 || colonne > 8 || ligne < 1 || ligne > 8) { // si on cherche à sortir de l'échiquier, on retourne faux
        return false;
    }
	if (e.getPiece(ligne, colonne) != NULL) {
        // Si la case destination n'est pas vide, on vérifie que la pièce qui l'occupe n'est pas de la même couleur,
        // et on vérifie également qu'on n'essaye pas de placer la pièce à sa position actuelle
		return (e.getPiece(ligne, colonne)->estBlanc() != estBlanc()
				&& (this->m_colonne != colonne || this->m_ligne != ligne));
	}
    return true;
}
