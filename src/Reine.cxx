#include <iostream>
#include "Reine.h"

using namespace std;

Reine::Reine(bool white) : Piece(white ? 1 : 8, 4, white), Fou(white, true), Tour(white, true) {}

bool Reine::mouvementValide(Echiquier &e, int x, int y) {
	return Fou::mouvementValide(e, y, x) || Tour::mouvementValide(e, y, x);
}

char Reine::codePiece() const {
	return m_white ? 'D' : 'd';
}