#include <Cavalier.h>
#include <Pion.h>
#include <Tour.h>
#include <Roi.h>
#include <Reine.h>
#include <iostream>
#include <stdexcept>
#include "Joueur.h"

using namespace std;

Joueur::Joueur() { }

Joueur::Joueur(bool white)
{
    Roi *roi = new Roi(white);
    Reine *reine = new Reine(white);
    Fou *fouGauche = new Fou(white, true);
    Fou *fouDroite = new Fou(white, false);
    Tour *tourGauche = new Tour(white, true);
    Tour *tourDroite = new Tour(white, false);
    Cavalier *cavalierGauche = new Cavalier(white, true);
    Cavalier *cavalierDroite = new Cavalier(white, false);

    m_pieces.push_back(roi);
    m_pieces.push_back(reine);
    m_pieces.push_back(fouGauche);
    m_pieces.push_back(fouDroite);
    m_pieces.push_back(tourGauche);
    m_pieces.push_back(tourDroite);
    m_pieces.push_back(cavalierGauche);
    m_pieces.push_back(cavalierDroite);

    for (int i = 1; i <= 8; i++) {
        Pion *pion = new Pion(i, white);
        m_pieces.push_back(pion);
    }
}

Joueur::~Joueur()
{
    vector<Piece *>::iterator p = m_pieces.begin();
    while (p != m_pieces.end()) {
        delete *p;
        p++;
    }
}

void Joueur::placerPieces(Echiquier &e)
{
    for (int i = 0; i < 16; i++)
        e.placer(m_pieces[i]);
}

void Joueur::jouerSonTour(Echiquier &e)
{
    int ligneSrc, colonneSrc;
    bool saisieOk = choisirPiece(ligneSrc, colonneSrc);
    while (!pieceValide(ligneSrc, colonneSrc, e) || !saisieOk) {
        cout << "Votre saisie est invalide, ou bien "
                        "vous ne pouvez pas choisir cette pièce. Veuillez réessayer." << endl;
        saisieOk = choisirPiece(ligneSrc, colonneSrc);
    }
    Piece *pieceSelectionnee = e.getPiece(ligneSrc, colonneSrc);

    int ligneDest, colonneDest;
    saisieOk = choisirDestinationPiece(ligneDest, colonneDest);
    while (!pieceSelectionnee->mouvementValide(e, ligneDest, colonneDest) || !saisieOk) {
        cout << "Impossible de déplacer la pièce sur cette case. Veuillez choisir une autre destination." << endl;
        saisieOk = choisirDestinationPiece(ligneDest, colonneDest);
    }

    e.deplacer(pieceSelectionnee, ligneDest, colonneDest);
}

bool Joueur::choisirDestinationPiece(int &ligne, int &colonne) const
{
    try {
        string ligneSaisie, colonneSaisie;
        cout << "Entrez les coordonnées de la destination de la pièce." << endl;
        cout << "Ligne : ";
        cin >> ligneSaisie;
        ligne = stoi(ligneSaisie);
        cout << "Colonne : ";
        cin >> colonneSaisie;
        colonne = stoi(colonneSaisie);
    }
    catch (const std::invalid_argument &e) {
        cout << "argument invalide" << endl;
        return false;
    }
    return true;
}

bool Joueur::choisirPiece(int &ligne, int &colonne) const
{
    try {
        string ligneSaisie, colonneSaisie;
        cout << "Sélectionnez une pièce de votre couleur." << endl;
        cout << "Ligne : ";
        cin >> ligneSaisie;
        ligne = stoi(ligneSaisie);
        cout << "Colonne : ";
        cin >> colonneSaisie;
        colonne = stoi(colonneSaisie);
    }
    catch (const std::invalid_argument &e) {
        return false;
    }
    return true;
}

bool Joueur::pieceValide(int ligne, int colonne, Echiquier &e)
{
    Piece *p;
    p = e.getPiece(ligne, colonne);
    return (p != NULL && p->estBlanc() == estBlanc());
}
