#include <iostream>
#include "Roi.h"

using namespace std;

Roi::Roi(bool white) : Piece(white ? 1 : 8, 5, white) {}

bool Roi::mouvementValide(Echiquier &e, int ligne, int colonne)
{
    if (!Piece::mouvementValide(e, ligne, colonne))
        return false;
	int dx = colonne - m_colonne;
    int dy = ligne - m_ligne;
	return (dx >= 0 && dx <= 1 && dy >= 0 && dy <= 1);
}

char Roi::codePiece() const {
	return m_white ? 'R' : 'r';
}