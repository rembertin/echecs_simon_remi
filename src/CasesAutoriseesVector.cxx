//
// Created by rembertin on 31/10/15.
//

#include "CasesAutoriseesVector.h"

CasesAutoriseesVector::CasesAutoriseesVector() : vector() {}

void CasesAutoriseesVector::ajouter(int ligne, int colonne)
{
    this->push_back(CaseAutorisee(ligne, colonne));
}


bool CasesAutoriseesVector::caseEstAutorisee(int ligne, int colonne)
{
    for (auto &caseAutorisee : *this) {
        if (caseAutorisee.getColonne() == colonne && caseAutorisee.getLigne() == ligne) {
            return true;
        }
    }
    return false;
}