#include <iostream>
#include <CasesAutoriseesVector.h>
#include "Pion.h"
#include "Echiquier.h"

using namespace std;

Pion::Pion(int x, bool white)
        : Piece(white ? 2 : 7, x, white) { }

bool Pion::mouvementValide(Echiquier &e, int ligne, int colonne)
{
    if (!Piece::mouvementValide(e, ligne, colonne))
        return false;

    CasesAutoriseesVector casesAutorisees = ajouterCasesAutorisees();
    return (casesAutorisees.caseEstAutorisee(ligne, colonne));
}


CasesAutoriseesVector Pion::ajouterCasesAutorisees()
{
    CasesAutoriseesVector casesAutorisees;
    if (estBlanc()) {
        casesAutorisees.ajouter(m_ligne + 1, m_colonne);
        casesAutorisees.ajouter(m_ligne + 2, m_colonne);
        casesAutorisees.ajouter(m_ligne + 1, m_colonne - 1);
        casesAutorisees.ajouter(m_ligne + 1, m_colonne + 1);
    }
    else {
        casesAutorisees.ajouter(m_ligne - 1, m_colonne);
        casesAutorisees.ajouter(m_ligne - 2, m_colonne);
        casesAutorisees.ajouter(m_ligne - 1, m_colonne - 1);
        casesAutorisees.ajouter(m_ligne - 1, m_colonne + 1);
    }
    return casesAutorisees;
}

char Pion::codePiece() const
{
    return m_white ? 'P' : 'p';
}