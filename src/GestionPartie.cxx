//
// Created by rembertin on 28/10/15.
//

#include <JoueurNoir.h>
#include <JoueurBlanc.h>
#include <iostream>
#include "GestionPartie.h"

using namespace std;

GestionPartie::GestionPartie()
{
    // Le joueur blanc commence la partie
    whiteMustPlay = true;
}

void GestionPartie::startGame()
{
    string titre =
            "EEEE   CCC  H   H  EEEE   CCC   SSS\n"
            "E     C     H   H  E     C     S\n"
            "EEE   C     HHHHH  EEE   C      SS\n"
            "E     C     H   H  E     C        S\n"
            "EEEE   CCC  H   H  EEEE   CCC  SSS\n";

    cout << titre << endl
         << "Bienvenue sur le jeu d'échecs en ligne de commande !" << endl
         << "Appuyez sur Entrée pour commencer." << endl;
    cin.get();
    Echiquier e;
    joueurBlanc.placerPieces(e);
    joueurNoir.placerPieces(e);

    while (!this->end()) {
        e.affiche();
        // On fait jouer alternativement le joueur blanc et le joueur noir
        this->tourSuivant(e);
    }
}

bool GestionPartie::end()
{
    return false;
}

void GestionPartie::tourSuivant(Echiquier &e)
{
    if (whiteMustPlay) {
        cout << "* Tour du joueur Blanc" << endl;
        joueurBlanc.jouerSonTour(e);
    }
    else {
        cout << "* Tour du joueur Noir" << endl;
        joueurNoir.jouerSonTour(e);
    }
    whiteMustPlay = !whiteMustPlay;
}