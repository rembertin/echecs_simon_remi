#include <cstdlib>
#include <GestionPartie.h>
// Pour utiliser les flux de iostream sans mettre "std::" tout le temps.
using namespace std;

/**
 * Programme principal
 */
int main(int argc, char **argv) {
    GestionPartie gestionPartie;
    gestionPartie.startGame();
}