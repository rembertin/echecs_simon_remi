#include <iostream>
// A besoin de la declaration de la classe
#include "Echiquier.h"

using namespace std;

/**
* Constructeur par défaut.
* Initialise à vide l'echiquier.
*/
Echiquier::Echiquier()
{
    for (int i = 0; i < 64; i++)
        m_cases[i] = NULL;
}

/**
* Recupere la piece situee sur une case.
*
* @param x un entier entre 1 et 8
* @param y un entier entre 1 et 8
*
* @return NULL si aucune piece n'est sur cette case et un pointeur
* vers une piece sinon.
*/
Piece *Echiquier::getPiece(int ligne, int colonne)
{
    if ((colonne >= 1) && (colonne <= 8) && (ligne >= 1) && (ligne <= 8))
        return m_cases[(colonne - 1) + 8 * (ligne - 1)];
    else
        return NULL;
}

/**
* Place une piece sur l'echiquier, aux coordonnees specifiees dans la piece.
*
* @param p un pointeur vers une piece
*
* @return 'true' si le placement s'est bien passe, 'false' sinon
* (case occupee, coordonnees invalides, piece vide )
*/
bool Echiquier::placer(Piece *p)
{
    if (p != NULL) {
        if (m_cases[(p->getColonne() - 1) + 8 * (p->getLigne() - 1)] == NULL
            && (p->getColonne() >= 1) && (p->getColonne() <= 8) && (p->getLigne() >= 1) && (p->getLigne() <= 8)) {
            m_cases[(p->getColonne() - 1) + 8 * (p->getLigne() - 1)] = p;
            return true;
        }
        return false;
    }
    return false;
}


/**
* Deplace une piece sur l'echiquier, des coordonnees specifiees
* dans la piece aux coordonnees x,y.
*
* @param p un pointeur vers une piece
* @param x un entier entre 1 et 8
* @param y un entier entre 1 et 8
*
* @return 'true' si le placement s'est bien passe, 'false' sinon
* (case occupee, coordonnees invalides, piece vide, piece pas
* presente au bon endroit sur l'echiquier)
*/
bool Echiquier::deplacer(Piece *piece, int ligne, int colonne)
{
    if (peutDeplacer(piece, ligne, colonne)) {
        int ancienneLigne = piece->getLigne(), ancienneColonne = piece->getColonne();
        piece->move(ligne, colonne); // On change les coordonnées de la pièce
        // La case sur laquelle était positionnée la pièce ne contient plus rien
        m_cases[(ancienneColonne - 1) + 8 * (ancienneLigne - 1)] = NULL;
        return placer(piece); // on déplace la pièce et on retourne vrai si ça s'est bien passé, faux sinon
    }
    return false;
}

bool Echiquier::peutDeplacer(Piece *piece, int ligne, int colonne)
{
    return (colonne > 0 && colonne <= 8 && ligne > 0 && ligne <= 8 && piece != NULL);
}

/**
* Affiche l'echiquier avec des # pour les cases noires et . pour
* les blanches si elles sont vides, et avec B pour les pieces
* blanches et N pour les pieces noires.
*/
void Echiquier::affiche()
{
    cout << endl << "   1 2 3 4 5 6 7 8" << endl;
    for (int ligne = 1; ligne <= 8; ++ligne) {
        cout << ligne << "  ";
        for (int colonne = 1; colonne <= 8; ++colonne) {
            char c;
            Piece *p = getPiece(ligne, colonne);
            if (p == NULL)
                c = ((colonne + ligne) % 2) == 0 ? '#' : '.';
            else
                c = p->codePiece(); // p->estBlanc() ? 'B' : 'N';
            cout << c << " ";
        }
        cout << " " << ligne << endl;
    }
    cout << "   1 2 3 4 5 6 7 8" << endl << endl;
}
