#include "JoueurNoir.h"
#include "Roi.h"
#include "Reine.h"
#include "Pion.h"
#include "Cavalier.h"

using namespace std;

JoueurNoir::JoueurNoir() : Joueur(false) {}

bool JoueurNoir::estBlanc() {
	return false;
}