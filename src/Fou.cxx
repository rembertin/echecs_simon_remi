#include "Fou.h"

Fou::Fou(bool white, bool left) : Piece(white ? 1 : 8, left ? 3 : 6, white) {}

bool Fou::mouvementValide(Echiquier &e, int ligne, int colonne)
{
	int dx = colonne - m_colonne; // déplacement en abscisses
    int dy = ligne - m_ligne; // déplacement en ordonnées
    return Piece::mouvementValide(e, ligne, colonne) && (dx == dy || dx == -dy); // si la position désirée est sur la diagonale de la position actuelle, on retourne vrai
}

char Fou::codePiece() const {
	return m_white ? 'F' : 'f';
}