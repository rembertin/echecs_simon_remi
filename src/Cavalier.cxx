#include <CasesAutoriseesVector.h>
#include "Cavalier.h"

Cavalier::Cavalier(bool white, bool left)
			: Piece(white ? 1 : 8, left ? 2 : 7, white) {}

bool Cavalier::mouvementValide(Echiquier &e, int ligne, int colonne)
{
	if (!Piece::mouvementValide(e, ligne, colonne)) {
		return false;
	}
    CasesAutoriseesVector casesAutorisees = ajouterCasesAutorisees();
    return casesAutorisees.caseEstAutorisee(ligne, colonne);
}

CasesAutoriseesVector Cavalier::ajouterCasesAutorisees() const {
    CasesAutoriseesVector casesAutorisees;
    casesAutorisees.ajouter(m_ligne - 2, m_colonne - 1);
    casesAutorisees.ajouter(m_ligne - 2, m_colonne + 1);
    casesAutorisees.ajouter(m_ligne - 1, m_colonne - 2);
    casesAutorisees.ajouter(m_ligne - 1, m_colonne + 2);
    casesAutorisees.ajouter(m_ligne + 2, m_colonne - 1);
    casesAutorisees.ajouter(m_ligne + 2, m_colonne + 1);
    casesAutorisees.ajouter(m_ligne + 1, m_colonne - 2);
    casesAutorisees.ajouter(m_ligne + 1, m_colonne + 2);
    return casesAutorisees;
}

char Cavalier::codePiece() const {
	return m_white ? 'C' : 'c';
}