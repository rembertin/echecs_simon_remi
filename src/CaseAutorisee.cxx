//
// Created by rembertin on 31/10/15.
//

#include "CaseAutorisee.h"

CaseAutorisee::CaseAutorisee(int ligne, int colonne) : m_colonne(colonne), m_ligne(ligne) {
}

CaseAutorisee::CaseAutorisee(int ligne, int colonne, bool peutPrendre) : CaseAutorisee(ligne, colonne)
{
    this->peutPrendre = peutPrendre;
}
