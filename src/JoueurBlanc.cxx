#include "JoueurBlanc.h"
#include "Roi.h"
#include "Reine.h"
#include "Pion.h"
#include "Cavalier.h"
#include "Piece.h"
#include <iostream>

using namespace std;

JoueurBlanc::JoueurBlanc() : Joueur(true) { }

bool JoueurBlanc::estBlanc() {
    return true;
}