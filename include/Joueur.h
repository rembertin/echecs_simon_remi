/**
 * Header de Joueur.cxx
 *
 * @file Joueur.h
 */

#if !defined Joueur_h
#define Joueur_h

#include "Piece.h"
#include "Echiquier.h"
#include <vector>

using namespace std;


/**
 * Représente un joueur qui 
 * peut être de couleur blanche
 * ou de couleur noir
 */
class Joueur {
protected:
	vector<Piece *> m_pieces;

public:
	~Joueur();
	/**
	 * @brief Constructeur par défaut
	 */
	Joueur();


 	/**
 	 * @brief constructeur spécifique
 	 * 
 	 * @param white permet de définir la couleur du joueur
 	 */
	Joueur(bool white);


	/**
	 * @brief place toutes les pièces du 
	 * joueur sur l'échiquier
	 * 
	 * @param e echiquier sur lequel placer les pièces
	 */
	void placerPieces(Echiquier &e);

	/**
	 * @brief Jouer un tour
	 * @details Permet de saisir les coordonées
	 * de la pièce choisie et de choisir
	 * l'emplacement de destination
	 * 
	 * @param e echiquier
	 */
    void jouerSonTour(Echiquier &e);

    /**
     * @brief Permet de choisir l'emplacement
     * de la pièce selectionnée
     * 
     * @param ligne numéro de la ligne de l'emplacement
     * @param colonne numéro de la colonne de l'emplacement
     * 
     * @return retourne true si le mouvement est valide
     * false sinon.
     */
	bool choisirDestinationPiece(int &ligne, int &colonne) const;

	/**
	 * @brief permet de choisir la pièce
	 * qui va être jouée
	 * 
     * @param ligne numéro de la ligne de l'emplacement
     * @param colonne numéro de la colonne de l'emplacement
	 * 
	 * @return retourne true si l'emplacement est valide
     * false sinon.
	 */
	bool choisirPiece(int &ligne, int &colonne) const;

	/**
	 * @brief permet de savoir si la piece
	 * est valide (de la bonne couleur)
	 * 
	 * @param ligne ligne de la pièce
	 * @param colonne colonne de la pièce
	 * @param e echiquier
	 * @return true si la pièce est valide false sinon
	 */
	bool pieceValide (int ligne, int colonne, Echiquier &e);

	virtual bool estBlanc() = 0;
};

#endif // Joueur_h
