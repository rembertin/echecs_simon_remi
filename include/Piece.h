/**
 * Header de Piece.cxx
 *
 * @file Piece.h
 */

#if !defined Piece_h
#define Piece_h

class Echiquier;

/**
 * Piece de jeu d'echec.
 */
class Piece
{
protected:
    int m_colonne;
    int m_ligne;
    bool m_white;

public:

    /**
     * @brief contructeur par defaut
     */
    Piece();

    /**
     * @brief contructer spécifique
     * 
     * @param ligne 
     * @param colonne
     * @param white définie la couleur de la pièce
     */
    Piece(int ligne, int colonne, bool white);

    /**
     * @brief constructeur par copie
     * 
     */
    Piece(const Piece &autre);


    /**
     * @brief redéfinition de l'opérateur
     * egal entre deux pièces
     * 
     * @param autre retourne une pièce
     */
    Piece &operator=(const Piece &autre);

    virtual ~Piece();

    /**
     * @brief definie la colonne et la ligne de la pièce
     * 
     * @param ligne
     * @param colonne
     */
    void move(int ligne, int colonne);

    /**
     * @brief permet de savoir si le mouvement est valide
     * 
     * @param e 
     * @param ligne
     * @param colonne
     * @return true si mouvement valide false si mouvement en
     * dehors de l'echiquier, si l'emplacement n'est pas vide.
     */
    virtual bool mouvementValide(Echiquier &e, int ligne, int colonne);

    int getLigne() const;

    int getColonne() const;

    /**
     * @brief permet de savoir la couleur du joueur
     * @return la variable m_white
     */
    bool estBlanc() const;

    /**
     * @brief définie la pièce
     * @return retourne le caractère à afficher sur l'échiquier
     */
    virtual char codePiece() const = 0;
};

#endif // !defined Piece_h
