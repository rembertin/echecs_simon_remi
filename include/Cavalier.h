#ifndef CAVALIER_H
#define CAVALIER_H

#include "Piece.h"

class CasesAutoriseesVector;

/**
 * Représente une pièce de type Cavalier
 */
class Cavalier : public Piece
{
public:
    /**
     * @brief Constructeur de Cavalier
     *
     * @param white Couleur du cavalier : true si blanc, false si noir
     * @param left [description]
     */
    Cavalier(bool white, bool left);

    bool mouvementValide(Echiquier &e, int ligne, int colonne);

    /**
     * @brief Renvoie le code de la pièce, selon la couleur du cavalier.
     * @return Le code de la pièce
     */
    char codePiece() const;

private:
    /**
     * @brief Remplit le vecteur de cases autorisées pour cette pièce
     * @return Le vecteur de cases autorisées créé
     */
    CasesAutoriseesVector ajouterCasesAutorisees() const;
};

#endif