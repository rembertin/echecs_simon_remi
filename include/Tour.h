#ifndef TOUR_H
#define TOUR_H

#include "Piece.h"

class Tour : virtual public Piece {
public:
	/**
	 * @brief contructeur spécifique
	 * 
	 * @param white couleur de la pièce
	 * @param left definie la localisation de la pièce
	 * (deux tours par couleur, à droite et à gauche)
	 */
    Tour(bool white, bool left);

    /**
     * @brief définie la validité du mouvement souhaité
     * 
     * @param e 
     * @param ligne
     * @param colonne
     * @return true si mouvement valide false sinon
     */
    bool mouvementValide(Echiquier &e, int ligne, int colonne);

    /**
     * @brief définie la pièce
     * @return retourne le caractère à afficher sur l'échiquier
     */
    char codePiece() const;
};

#endif