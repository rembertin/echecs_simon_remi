#ifndef FOU_H
#define FOU_H

#include "Piece.h"

/**
 * Représente une pièce de type Fou
 */
class Fou : virtual public Piece
{
public:
    /**
     * @brief Constructeur de Fou
     *
     * @param white Indique la couleur du Fou
     * @param left Indique s'il s'agit du Fou de gauche ou de droite
     */
    Fou(bool white, bool left);

    bool mouvementValide(Echiquier &e, int ligne, int colonne);

    char codePiece() const;
};

#endif