#ifndef JOUEURBLANC_H
#define JOUEURBLANC_H

#include "Joueur.h"

class JoueurBlanc : public Joueur {
public:
	/**
	 * @brief contructeur par defaut
	 */
    JoueurBlanc();

    /**
     * @brief permet de savoir la couleur du joueur
     * @return true
     */
    bool estBlanc();
};

#endif