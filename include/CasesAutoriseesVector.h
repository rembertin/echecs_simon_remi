//
// Created by rembertin on 31/10/15.
//

#ifndef ECHECS_PROJET_CASESAUTORISEESLIST_H
#define ECHECS_PROJET_CASESAUTORISEESLIST_H

#include <vector>
#include "CaseAutorisee.h"

/**
 * @brief Représente un vecteur de cases autorisées
 */
class CasesAutoriseesVector : public std::vector<CaseAutorisee>
{
public:
    /**
     * @brief Constructeur par défaut
     */
    CasesAutoriseesVector();

    /**
     * @brief Ajoute une case autorisée
     * 
     * @param ligne Numéro de ligne
     * @param colonne Numéro de colonne
     */
    void ajouter(int ligne, int colonne);

    /**
     * @brief Indique si une case est autorisée
     * 
     * @param ligne Numéro de ligne
     * @param colonne Numéro de colonne
     * 
     * @return true si la case est autorisée, faux sinon
     */
    bool caseEstAutorisee(int ligne, int colonne);
};


#endif //ECHECS_PROJET_CASESAUTORISEESLIST_H
