//
// Created by rembertin on 31/10/15.
//

#ifndef ECHECS_PROJET_CASEAUTORISEE_H
#define ECHECS_PROJET_CASEAUTORISEE_H

/**
 * Représente une case dans lequel le déplacement est autorisé
 */
class CaseAutorisee
{
public:
    /**
     * @brief Constructeur de CaseAutorisee
     * 
     * @param ligne Numéro de ligne
     * @param colonne Numéro de colonne
     */
    CaseAutorisee(int ligne, int colonne);

    /**
     * @brief Constructeur de CaseAutorisee
     * 
     * @param ligne Numéro de ligne
     * @param colonne Numéro de colonne
     * @param peutPrendre Indique si cette case peut être prise
     */
    CaseAutorisee(int ligne, int colonne, bool peutPrendre);

    int getLigne() const
    {
        return m_ligne;
    }

    int getColonne() const
    {
        return m_colonne;
    }

private:
    /**
     * Numéro de ligne de la case
     */
    int m_ligne;

    /**
     * Numéro de colonne de la case
     */
    int m_colonne;

    /**
     * Indique si cette case peut être prise par l'adversaire
     */
    bool peutPrendre = true;
};

#endif //ECHECS_PROJET_CASEAUTORISEE_H