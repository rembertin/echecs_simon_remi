#ifndef PION_H
#define PION_H

#include "Piece.h"

class CasesAutoriseesVector;

class Pion : public Piece
{
public:
	/**
	 * @brief contructeur spécifique
	 * 
	 * @param x emplacement du pion sur sa ligne
	 * @param white couleur du pion
	 */
	Pion(int x, bool white);
	
	/**
	 * @brief définie si le mouvement est
	 *  valide pour le pion
	 * 
	 * @param e 
	 * @param ligne 
	 * @param colonne 
	 * @return true si mouvement valide false sinon
	 */
	bool mouvementValide(Echiquier &e, int ligne, int colonne);

	/**
     * @brief définie la pièce
     * @return retourne le caractère à afficher sur l'échiquier
     */
	char codePiece() const;

	/**
	 * @brief définition des mouvement du pion
	 * @return les cases disponible selon le déplacement d'un pion
	 */
    CasesAutoriseesVector ajouterCasesAutorisees();
};

#endif