#ifndef REINE_H
#define REINE_H

#include "Fou.h"
#include "Tour.h"

class Reine : public Tour, public Fou {
public:
	/**
	 * @brief contructeur spécifique
	 * 
	 * @param white couleur de la pièce
	 */
    Reine(bool white);

    /**
     * @brief définie la validité du mouvement souhaité
     * 
     * @param e 
     * @param x ligne
     * @param y colonne
     * @return true si mouvement valide false sinon
     */
    bool mouvementValide(Echiquier &e, int x, int y);

	/**
     * @brief définie la pièce
     * @return retourne le caractère à afficher sur l'échiquier
     */
    char codePiece() const;
};

#endif