//
// Created by rembertin on 28/10/15.
//

#ifndef ECHECS_PROJET_GESTIONPARTIE_H
#define ECHECS_PROJET_GESTIONPARTIE_H

#include "JoueurBlanc.h"
#include "JoueurNoir.h"

/**
 * Représente l'arbitre de la partie : 
 * gère le début de la partie et la 
 * gestion des tours de jeu
 */
class GestionPartie
{
public:
    /**
     * @brief Constructeur par défaut
     */
    GestionPartie();

    /**
     * @brief Démarre le jeu
     */
    void startGame();

    /**
     * @brief Indique si le jeu est terminé ou non
     * @return True si le jeu est terminé, false sinon
     */
    bool end();

private:
    JoueurBlanc joueurBlanc;
    JoueurNoir joueurNoir;

    void tourSuivant(Echiquier &e);

    bool whiteMustPlay;
};


#endif //ECHECS_PROJET_GESTIONPARTIE_H
