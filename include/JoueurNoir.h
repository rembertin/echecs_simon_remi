#ifndef JOUEURNOIR_H
#define JOUEURNOIR_H

#include "Joueur.h"

class JoueurNoir : public Joueur {
public:
	/**
	 * @brief contructeur par defaut
	 */
	JoueurNoir();

    /**
     * @brief permet de savoir la couleur du joueur
     * @return true
     */
	bool estBlanc();
};

#endif

