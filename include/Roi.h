#ifndef ROI_H
#define ROI_H

#include "Piece.h"

class Roi : public Piece {
public:
	/**
	 * @brief contructeur spécifique
	 * 
	 * @param white couleur de la pièce
	 */
    Roi(bool white);

    /**
     * @brief définie la validité du mouvement souhaité
     * 
     * @param e 
     * @param ligne
     * @param colonne
     * @return true si mouvement valide false sinon
     */
    bool mouvementValide(Echiquier &e, int ligne, int colonne);

    /**
     * @brief définie la pièce
     * @return retourne le caractère à afficher sur l'échiquier
     */
    char codePiece() const;
};

#endif