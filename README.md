*** Projet de jeu d'échecs en C++ ***

Bienvenue cher collaborateur ! Vous trouverez ci dessous quelques informations pratiques pour le projet.

	- Les sources, au format .cxx sont situées dans le dossier src/
	- Les headers, au format .h sont situées dans le dossier include/
	- Pour compiler le projet, il suffit d'entrer la commande "make" dans un terminal
	- Pour nettoyer le projet, faites "make clean"
	- Pour lancer l'executable, faites "make start"

Pour développer avec Sublime Text, voici quelques packages intéressants à installer :
	- C++YouCompleteMe : autocompletion pour le C++
	- DoxyDoc : permet de créer des commentaires en bloc pour la documentation du code, comme de la JavaDoc
	- Git : intégration de Git à Sublime Text
	- SublimeAStyleFormatter : plugin pour reformater le code selon la norme AStyle

IMPORTANT : Avant de commit, veuillez faire un "make clean" pour garder le projet propre.