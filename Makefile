CC=g++
CFLAGS=-I include -std=c++11
SOURCES=src/*.cxx
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=echecs

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	@$(CC) $(CFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $< -o $@

clean:
	@rm -f $(EXECUTABLE)

start:
	./$(EXECUTABLE)	